<?php
/*
 * @package mod_naas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../config.php");
require_once($CFG->dirroot.'/mod/lti/lib.php');
require_once($CFG->dirroot.'/mod/lti/locallib.php');
require_once('classes/NaasClient.php');

// Get data from DB
$id = required_param('id', PARAM_INT); // Course Module ID.
$triggerview = optional_param('triggerview', 1, PARAM_BOOL);

$cm = get_coursemodule_from_id('naas', $id, 0, false, MUST_EXIST);
$naas_instance = $DB->get_record('naas', array('id' => $cm->instance), '*', MUST_EXIST);

$context = context_module::instance($cm->id);

$course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);

// Check credentials
require_login($course, true, $cm);
require_capability('mod/naas:view', $context);

// Retrieve LTI config from NaaS server
$config = get_config('naas');
$naas = new NaasClient($config);
$nugget_config = $naas->get_nugget_lti_config($naas_instance->nugget_id);
if ($nugget_config==null || array_key_exists("error", $nugget_config)) {
    error_log("Cannot get nugget information from Naas server");
    echo("Cannot get nugget information from NaaS server");
    return;
}

// Conigure LTI module
$PAGE->set_course($course);
$naas_instance->cmid = $cm->id;
$naas_instance->course = $course->id;
$naas_instance->toolurl = $nugget_config->url;
$naas_instance->securetoolurl = $nugget_config->url;
$naas_instance->password = $nugget_config->secret;
$naas_instance->resourcekey = $nugget_config->key;
$naas_instance->debuglaunch = 0;
$custom = [
	"css" => $config->naas_css
];
$naas_instance->instructorcustomparameters = "naas=". json_encode($custom);

# TODO: permettre de configurer ces paramètes de sécurité
$naas_instance->instructorchoicesendname = 1;
$naas_instance->instructorchoicesendemailaddr = 1;
$naas_instance->instructorchoiceacceptgrades = 1;
$naas_instance->instructorchoiceallowroster = null;

// Launch the LTI module
lti_launch_tool($naas_instance);

