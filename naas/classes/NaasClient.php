<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Push to naas
 *
 * @package    naas
 * @copyright  2019 onwards ISAE-SUPAERO
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


class NaasClient  {
     /**
     * @var Class that enables interaction with a NaaS Server
     */
    protected $config;
    protected $debug;


    public function __construct($config) { 
        $this->config = $config;
        if (property_exists($this->config, "naas_debug")) $this->debug = $this->config->naas_debug;
        else $this->debug = False;
    }
    function log($thing) {
        error_log("[NaaS] ".print_r($thing, 1));
    }
    function debug($thing) {
        if ($this->debug) $this->log($thing);
    }
    // Makes a curl file from a moodle file
    function make_curl_file($file) {

        $mime = mime_content_type($file);
        $name = basename($file);
        return new \CURLFile($file, $mime, $name);
    }
    // Makes an HHTP request returns the json decoded body or null in case of http error
    function request($protocol, $service, $data = null, $params = null) {
        $url = $this->config->naas_endpoint.$service;
        if ($params != null) {
            $url = $url."?".http_build_query($params);
        }
        $ch = curl_init();
        $headers = [];
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $this->config->naas_username. "@".$this->config->naas_structure_id.":" . $this->config->naas_password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        if (property_exists($this->config, "naas_timeout")) {
            curl_setopt($ch, CURLOPT_TIMEOUT, $this->config->naas_timeout);
        }
        if (array_key_exists("naas_impersonate", $this->config)) {
            $headers[] = "X-NaaS-Impersonate:" .$this->config->naas_impersonate;
        } 
        if ($protocol=="FILE") {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        } else {
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $protocol);
            if ($protocol=="PUT" || $protocol=="POST") {
              $data_json = json_encode($data);
              $headers[] = 'Content-Length:' . strlen($data_json);                                
              curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
            }
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $body = curl_exec($ch);
        $code = curl_getinfo ( $ch, CURLINFO_RESPONSE_CODE );
        $content_type = curl_getinfo ($ch, CURLINFO_CONTENT_TYPE );
        if(curl_errno($ch)) {
            echo 'Curl error: ' . curl_error($ch);
        }
        if ($code!=200) {
            $this->log("Request failed: ".$protocol." - ".$url." (".$code.")");
            if(curl_errno($ch)) {
                $this->log('=> Curl error: ' . curl_error($ch));
            }
            $this->debug($body);
        }
        curl_close($ch);
      
        $result = $body;
        $result = json_decode($body);
       
        return $result;
    }
    function is_error($res) {
        return ($res==null || 
            (is_object($res) && array_key_exists("error", $res)));
    }
    // Uploads nugget files using one of the endpoints
    function upload_file_be($file, $service) {
        $protocol = "FILE";
        $data = [ 
            "file" => $this->make_curl_file($file)
        ];

        $res = $this->request($protocol, $service, $data);
        if (!$this->is_error($res))
            return $res->payload;
        else {
            $this->log("File upload failed");
            return null;
        }
    }
    // Uploads a file to the naas for safekeeping
    function upload_file($file) {
        return $this->upload_file_be($file, "/blobs");
    }
    // Uploads a file to the naas for hashing
    function hash_file($file) {
        return $this->upload_file_be($file, "/blobs/hash");
    }
    //  Prepare the data to be sent to the NaaS in case of creation of update of a nugget.
    function prepare_post_data($nugget) {
        // Uploads the file on the NaaS
        $files = $nugget->file;
        $hash_tot = "";
        $error = False;
        $nugget->hash = null;
        // Upload h5p files
        foreach ($files as $index => $filepath) {
            # get hash from first file
            if ($nugget->hash==null) {
                $nugget->hash = $this->hash_file($filepath);
                if ($nugget->hash == null) {
                    $this->log("Hash calculation failed");
                    return null;
                } else {
                    $this->debug("Hash calculation ok: ".$nugget->hash);
                }
            }
            $payload = $this->upload_file($filepath);
            $file_id = $payload;
            // Compute $data
            if ($file_id!=null) {
                $nugget->file_id[$index] = $file_id;
                $this->debug("File upload ok: ".$file_id);
            }
            else { 
                $this->log("Data preparation failed");
                return null;
            }
        }
        if ($nugget->thumbnail!=null) {
            $nugget->thumbnail_id = $this->upload_file($nugget->thumbnail);
            if ($nugget->thumbnail_id!=null) {
                $this->debug("Thumbnail upload ok:".$nugget->thumbnail_id);
            }
            else {
                $this->log("Thumbnail upload failed");
                return null;   
            }
        }
        $this->debug("prepare_post_data succeeded");
        return $nugget;
    }
    function handle_result($res) {
        if ($res!=null) {
            if (array_key_exists("payload", $res) && $res->payload!=null) {
                $this->debug("Payload: ".print_r($res->payload,1));
                return $res->payload;
            }
            else if (array_key_exists("error", $res)) {
                $this->debug($res->error);
            } else $this->debug("Unexpected_error");
        }
        else {
            $this->debug("Unexpected_error");
        }
        return $res;           
    }
    // Retrieve the LTI config of a nugget from the NaaS
    function get_nugget_lti_config($nugget_id) {
        $this->debug("Get nugget LTI config: ".$nugget_id);
        $protocol = "GET";
        $service = "/nuggets/".$nugget_id."/lti";
        $config = $this->request($protocol, $service);
        return $this->handle_result($config);
    }
    // Retrieve nugget versions from the NaaS
    function get_nugget_versions($nugget_id) {
        $this->debug("Get nugget versions: ".$nugget_id);
        $protocol = "GET";
        $service = "/nuggets/".$nugget_id."/versions";
        $result = $this->request($protocol, $service);
        return $this->handle_result($result);
    }
    // Retrieve the nugget metadata from the NaaS
    function get_nugget($nugget_id) {
        $this->debug("Get nugget: ".$nugget_id);
        $protocol = "GET";
        $service = "/nuggets/".$nugget_id;
        $result = $this->request($protocol, $service);
        return $this->handle_result($result);
    }
    // Adds a nugget to the NaaS
    function add_nugget($nugget) {        
        $this->debug("Adding nugget");
        $protocol = "POST";
        $service = "/nuggets";
        $data = $this->prepare_post_data($nugget);
        if ($data == null) return null;
        $result = $this->request($protocol, $service, $data);
        return $this->handle_result($result);
    }
    // Updates an existing nugget in the NaaS
    function update_nugget($nugget_id, $nugget) {
        $this->debug("Updating nugget: ".$nugget_id);
        $protocol = "PUT";
        $service = "/nuggets/".$nugget_id;
        $data = $this->prepare_post_data($nugget);
        if ($data == null) return null;
        $result = $this->request($protocol, $service, $data);
        return $this->handle_result($result); 
    }
    // Applies a simple action on agiven nugget
    function action_nugget_version($action, $version_id) {
        $this->debug("Action on nugget(".$action."): ".$version_id);
        $protocol = "POST";
        $service = "/versions/".$version_id."/".$action;
        $result = $this->request($protocol, $service, null);
        return $this->handle_result($result);
    }
    // Get the preview URL of a nugget version
    function get_version_preview_url($version_id) {
        $this->debug("Getting nugget version preview url: ".$version_id);
        $protocol = "GET";
        $service = "/versions/".$version_id."/preview_url";
        $result = $this->request($protocol, $service, null);
        return $this->handle_result($result);
    }    
    // Query nuggets available on the NaaS
    function query_nuggets($params) {
        $protocol = "GET";
        $service = "/nuggets";
        $nuggets = $this->request($protocol, $service, null, $params);
        return $this->handle_result($nuggets);
    }
    // Search nuggets available on the NaaS
    function search_nuggets($params) {
        $protocol = "GET";
        $service = "/nuggets/search";
        $result = $this->request($protocol, $service, null, $params);
        return $this->handle_result($result);
    }


    // Retrieve the nugget metadata from the NaaS (default version)
    function get_default_nugget($nugget_id) {
        $this->debug("Get nugget: ".$nugget_id);
        $protocol = "GET";
        $service = "/nuggets/".$nugget_id."/default_version";
        $result = $this->request($protocol, $service);
        return $this->handle_result($result);
    }
}
