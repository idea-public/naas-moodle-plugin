<?php


namespace mod_naas;

class NaasAuthor {
    public function __construct($endpoint, $cookie) { 
        $this->endpoint = $endpoint;
        $this->cookie = $cookie;
    }
    function get_stored_file_path($file) {
        $file_handle = $file->get_content_file_handle();
        $meta_data = stream_get_meta_data($file_handle);
        $file_path = $meta_data["uri"];
        return $file_path;
    }

    function make_curl_file($file) {
        $path = $this->get_stored_file_path($file);
        $mime = mime_content_type($path);
        $name = basename($path);
        return new \CURLFile($path, $mime, $name);
    }
    function request($protocol, $service, $data = null, $params = null) {
        $url = $this->endpoint.$service;
        if ($params != null) {
            $url = $url."?".http_build_query($params);
        }
        $ch = curl_init();
        $headers = [
            "Cookie: ".$this->cookie
        ];
        curl_setopt($ch, CURLOPT_URL, $url);
        #curl_setopt($ch, CURLOPT_USERPWD, $this->config->naas_user. ":" . $this->config->naas_password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        if ($protocol=="FILE") {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        } else {
            //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $protocol);
            if ($protocol=="PUT" || $protocol=="POST") {
              //$headers[] = 'Content-Length: ' . strlen($data_json);    
              curl_setopt($ch, CURLOPT_POST, true);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);

            }
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $body = curl_exec($ch);
        $code = curl_getinfo ( $ch, CURLINFO_RESPONSE_CODE );
        $content_type = curl_getinfo ($ch, CURLINFO_CONTENT_TYPE );
            error_log("Naas: Request failed: ".$protocol." - ".$url." (".$code.")");
        if ($code>=399) {
            error_log("Naas: Request failed: ".$protocol." - ".$url." (".$code.")");
            error_log($body);
            return null;
        }
        
        curl_close($ch);
        $result = $body;
       
        //error_log("OK: ".print_r($result,true));
        return $result;
    }

    public function get_form_input_value($xpath, $name) {
        $query = "//input[@name='" . $name. "']";
        $entries = $xpath->query($query);
        return $entries[0]->getAttribute('value');
    }
    # Télécharge un formulaire de création de nugget et récuère les paramètres d'entrée
    public function get_form_inputs() {
        $protocol = "GET";
        $service = "/node/add/nugget";
        $body = $this->request($protocol, $service, null, null);
        $parser = xml_parser_create();
        error_reporting (0);
        $doc = new \DOMDocument();
        $doc->loadHTML($body);
        $xpath = new \DOMXPath($doc);
        error_reporting (E_WARNING);

        
        return [
            "form_id" => $this->get_form_input_value($xpath, "form_id"),
            "form_token" => $this->get_form_input_value($xpath, "form_token"),
            "form_build_id" => $this->get_form_input_value($xpath, "form_build_id"),
            "changed" => $this->get_form_input_value($xpath, "changed"),
        ];
    }
    public function get_user_info() {
        $protocol = "FILE";
        $service = "/user/login";
        $body = $this->request($protocol, $service);
    }
    # Upload un fichier H5P dans drupal
    public function upload_h5p_file($file) {
        $data = [ 
            "h5p" => $this->make_curl_file($file)
        ];
        $protocol = "FILE";
        $service = "/h5peditor/c87f9f2426e9c/0/library-upload";
        //$service = "/node/add/nugget";
        //$protocol = "GET";
        $res = $this->request($protocol, $service, $data);
            echo("Cannot upload H5P file");
        if ($res == null) { 
            echo("Cannot upload H5P file");
        }
        return json_decode($res)->data;
    }
    public function create_nugget_in_author($data) {
        $protocol = "POST";
        $service = "/node/add/nugget";
        return $this->request($protocol, $service, $data);
    }
    public function export_h5p($nugget_data, $file) {
        //$data = $this->get_user_info();
        $form_data = $this->get_form_inputs();
        $upload_data = $this->upload_h5p_file($file);
        $data = $form_data;
        $h5p_parameters = [ "params" => $upload_data->content ];
        $data["h5p[0][h5p_content][parameters]"] = json_encode($h5p_parameters);
        $data["h5p[0][h5p_content][library]"] = $nugget_data["library"];//$upload_data->h5p->mainLibrary
        $data["h5p[0][h5p_content][frame]"] = "1";
        $data["h5p[0][h5p_content][copyright]"] ="1";

        $data["title[0][value]"] = $nugget_data["title"];
        $data["resume[0][value]"] = $nugget_data["resume"];
        $data["revision_log[0][value]"] = "";
        $data["advanced__active_tab"] = "edit-revision-information";
        $data["moderation_state[0][state]"] = "draft";
        $data["op"] = $nugget_data["op"];
        echo(print_r($data));
        $this->create_nugget_in_author($data);
        
        
    }
}