<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
//
// This file is part of BasicLTI4Moodle
//
// BasicLTI4Moodle is an IMS BasicLTI (Basic Learning Tools for Interoperability)
// consumer for Moodle 1.9 and Moodle 2.0. BasicLTI is a IMS Standard that allows web
// based learning tools to be easily integrated in LMS as native ones. The IMS BasicLTI
// specification is part of the IMS standard Common Cartridge 1.1 Sakai and other main LMS
// are already supporting or going to support BasicLTI. This project Implements the consumer
// for Moodle. Moodle is a Free Open source Learning Management System by Martin Dougiamas.
// BasicLTI4Moodle is a project iniciated and leaded by Ludo(Marc Alier) and Jordi Piguillem
// at the GESSI research group at UPC.
// SimpleLTI consumer for Moodle is an implementation of the early specification of LTI
// by Charles Severance (Dr Chuck) htp://dr-chuck.com , developed by Jordi Piguillem in a
// Google Summer of Code 2008 project co-mentored by Charles Severance and Marc Alier.
//
// BasicLTI4Moodle is copyright 2009 by Marc Alier Forment, Jordi Piguillem and Nikolas Galanis
// of the Universitat Politecnica de Catalunya http://www.upc.edu
// Contact info: Marc Alier Forment granludo @ gmail.com or marc.alier @ upc.edu.

/**
 * This file sends all h5p content to a drupal "Naas auteur" instance
 *
 * @package mod_naas
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/*

On the drupal instance h5p token security check must be disabled security
mod/hvp/editor/h5peditor-ajax.class.php l 226
*/
require_once("../../config.php");
require_once('classes/NaasMoodle.php');
require_once('classes/NaasAuthor.php');
require_once($CFG->dirroot.'/config.php');

$activities = $DB->get_records('hvp');
$export_dir = "/home/bilponse/tmp/h5p";
$author_role_id = 3;

function store_file($file) {
    global $naas_moodle;
    global $export_dir;
    $orig_filename = $file->get_filename();
    $extension = pathinfo($orig_filename, PATHINFO_EXTENSION);
    $filepath = $naas_moodle->get_stored_file_path($file);
    $filename = basename($filepath) . ".". $extension;
    copy($filepath, "$export_dir/$filename");
    return $filename;
}
$naas_moodle = new \mod_naas\NaasMoodle();
$i = 0;
$metadata = [];
$activitynb = count($activities);
foreach ($activities as $activity ) {
	$i ++;
    # get metadata
    $course = $DB->get_record('course', ['id' => $activity->course]);
    $context = context_course::instance($course->id);

    error_log("Nugget export: $course->fullname $activity->id $i/$activitynb");

    $enrolment = $DB->get_record('enrol', ['courseid' => $course->id, 'enrol'=> 'manual']);
    $role_assignments = $DB->get_records('role_assignments', [
        'contextid' => $context->id, 'roleid' => $author_role_id]);
    $get_username = function($role_assignment) {
        global $DB;
        $user = $DB->get_record('user', ['id' => $role_assignment->userid]);
        return ["name" => "$user->firstname $user->lastname",
                "mail" => $user->email,
                "login" => $user->username ];
    };
    $authors = array_map($get_username, $role_assignments);
    
    $activityid = $activity->id;

	# copy h5p file
    $imgfilename = store_file($naas_moodle->get_course_img($context->id));
    $h5pfilename = store_file($naas_moodle->get_hvp_file($activity->id, $context->id));


    # write metadata file
    $metadata[$h5pfilename] = [
        "image_file" => $imgfilename,
    	"title" => $course->fullname,
        "h5p_title" => $activity->name,
    	"resume" => $course->summary,
        "auteurs" => array_values($authors),
        "source" => "$CFG->wwwroot/course/view.php?id=$course->id"
    ];
    error_log(json_encode($metadata));
}
$metadata_file = fopen("$export_dir/metadata", "w");
fwrite($metadata_file, json_encode($metadata));

