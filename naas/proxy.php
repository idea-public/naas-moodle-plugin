<?php

// Proxy.php can be used for the NaaS API requests
// It uses an instance of the NaasClient.php to send the requests

// Accepts two kinds of headers:
// 1- ?fulltext=...&is_default_version=...&page_size=...
// 2- ?nugget_id=...


	require_once('classes/NaasClient.php');
	require_once("../../config.php");

	$config = get_config('naas');
	$naas = new NaasClient($config);


	if (isset($_GET['fulltext']) AND isset($_GET['is_default_version']) AND isset($_GET['page_size'])) 
	{
		// search nugget with fulltext 
        $nuggets = $naas->search_nuggets([ "is_default_version" => $_GET['is_default_version'],
                                         "page_size" => $_GET['page_size'],
                                         "fulltext" => $_GET['fulltext']
        ])->items;

        echo json_encode($nuggets);

	} else if (isset($_GET['nugget_id'])){

			$nugget = $naas->get_default_nugget($_GET['nugget_id']);
			echo json_encode($nugget);

	} else 	{
		echo 'Demande erronée';
	}




?>