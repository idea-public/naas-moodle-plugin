/**
 * @copyright 2021 Alban Dupire <dupire.alban@gmail.com>
 */

  // Card component to display basic nugget  information
  Vue.component('nugget-post', {
  props: ['post'],
  template: ` <div class="nugget-post">  
                  <img style="width:100%" :src="post.nugget_thumbnail_url.concat('?width=700&height=700')" alt="">   
                  <h3>{{ post.name }}</h3>  
                  <p> {{ post.resume | truncate(190, \'...\')}} </p>
                  <p> {{post.displayinfo}} </p>  
              </div>
              `
});


  // Retrieve stings or file roots 
  var label = document.getElementById('getVariable').value;
  var labelJson = JSON.parse(label.replaceAll('\'','\"'));
  document.getElementById('nugget_search').innerHTML = labelJson.nugget_search;
  document.getElementById('nugget_search_info1').innerHTML = labelJson.nugget_search_info1;
  document.getElementById('nugget_search_info2').innerHTML = labelJson.nugget_search_info2;
  document.getElementById('nugget_search_info3').innerHTML = labelJson.nugget_search_info3;
  var url_proxy = labelJson.url_proxy;
  var url_root = document.getElementById('getWWWRoot').value;



  var app = new Vue({
     el: '#app',
     data: {
     info: 'information',
     typed: '',
     simpleSuggestionList : [],
     suggestionList : [],
     posts: [],
     selected_id: null,
     selected_nugget: null
     },
    methods : {
      initialize () {
        this.selected_id = document.getElementsByName('nugget_id')[0].value;
        if (this.selected_id == ''){
            this.posts = [];
          } else {
            // Nugget_id in memory -> Retrieve from id
            this.createSimpleAxiosRequest(url_proxy + '?nugget_id=' + this.selected_id, true);  
          }
      },
      onInput () {
        if (this.typed.length > 2){
            this.requestNuggetSearch();
        } else if (this.typed.length == 0){
          this.initialize();
        }
      },
      requestNuggetSearch () {
        this.createSimpleAxiosRequest(url_proxy + '?fulltext=' + this.typed + '&is_default_version=true&page_size=9', false);
      },
      clickOnNugget: function (post) {
        event.preventDefault();

        if (this.selected_id == post.nugget_id) {
            document.getElementById('id_name').value = '';
            document.getElementsByName('nugget_id')[0].value = '';
            this.selected_nugget = null;
            this.selected_id = '';
        } else {
            document.getElementById('id_name').value = post.name;
            document.getElementsByName('nugget_id')[0].value = post.nugget_id;
            this.selected_nugget = post;
            this.selected_id = post.nugget_id;
        }

     },
     createSimpleAxiosRequest: function (urlRequest, initialize){

        const myaxios = axios.create({ baseURL: url_root });
        myaxios.get(urlRequest)
            .then(response => {
            	if (response.data.length == 0) { 
                this.posts = [];
            	} else {
                  // La requete a envoyé un ou plusieurs nugget(s)
	                if (Array.isArray(response.data)){
	                  this.posts = response.data; 
	                } else {
	                  this.posts = [response.data];
                    if (initialize) {this.selected_nugget = response.data};
	                }

        
            	}

                  // Si nugget sélectionné -> le faire apparaître (mais pas en double)
                  if (this.selected_nugget != null){
                    for(var i=0; i < this.posts.length; i++){
                      if(this.posts[i].nugget_id == this.selected_id){
                          this.posts.splice(i, 1);
                          break;
                      }
                    }
                    this.posts.unshift(this.selected_nugget);
                  }

              });
     }
   },
   mounted : function (){
    this.initialize();
   }
  });

        function reqListener () {
          alert(this.responseText);
        }


Vue.filter('truncate', function (text, length, suffix) {
    if (text.length > length) {
        return text.substring(0, length) + suffix;
    } else {
        return text;
    }
});


