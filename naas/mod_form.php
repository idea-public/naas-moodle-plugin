<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * NaaS configuration form
 *
 * @package    mod_naas
 * @copyright  2019 Bruno Ilponse
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once ($CFG->dirroot.'/course/moodleform_mod.php');
require_once('classes/NaasClient.php');



class mod_naas_mod_form extends moodleform_mod {
    function sort_by_name($a, $b) {
      // Compares two nuggets (sort by name)
      if ($a->name == $b->name) {
          return 0;
      }
      return ($a->name < $b->name) ? -1 : 1;
    }

    function definition() {
        global $CFG, $DB;
        $mform = $this->_form;

        $config = get_config('naas');
        $naas = new NaasClient($config);

        $nuggets = $naas->search_nuggets([ "is_default_version" => True,
                                         "is_published" => True
        ])->items;

        if ($nuggets == null) {
            $mform->addElement('html', '<div class="alert alert-danger">Impossible de contacter le serveur NaaS</div>');
        }

        //-------------------------------------------------------
        $mform->addElement('header', 'general', get_string('general', 'form'));
        



    $label_test = file_get_contents($CFG->wwwroot . '/mod/naas/assets/label.txt');



    $label = file_get_contents($CFG->wwwroot . '/mod/naas/assets/label.txt');
    $html = file_get_contents($CFG->wwwroot . '/mod/naas/assets/nuggetSearchWidget.html');
    $html = str_replace('$$place_holder1$$', $CFG->wwwroot, $html); 
    $html = str_replace('$$place_holder2$$', $label, $html); 
    $mform->addElement('html', $html );


        $mform->addElement('text', 'name', 'Nom à afficher', array('size'=>'48'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addElement('hidden', 'nugget_id', '', array('size'=>'48'));
        $mform->setType('nugget_id', PARAM_TEXT);
        $mform->addRule('nugget_id', null, 'required', null, 'client');



/*



// style par defailt pour le vue single select
<link rel="stylesheet" href="https://unpkg.com/vue-simple-suggest/dist/styles.css">

*/


        // Description du cours (à remettre)
        // $this->standard_intro_elements();
        //-------------------------------------------------------
        $this->standard_coursemodule_elements();

        //-------------------------------------------------------
        $this->add_action_buttons();
    }

    function data_preprocessing(&$default_values) {
    }

    function validation($data, $files) {
        $errors = parent::validation($data, $files);
        return $errors;
    }

}

