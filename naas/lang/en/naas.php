<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'url', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package    mod_naas
 * @copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'NaaS';
$string['modulename'] = 'Nugget';
$string['modulename_help'] = 'The Nugget module enables a teacher to instantiate a ressource provided by a NaaS server';
$string['modulename_link'] = 'mod/naas/view';
$string['modulenameplural'] = 'Nuggets';
$string['pluginadministration'] = '';
$string['nugget'] = 'Nugget';

$string['naas_settings'] = 'NaaS settings';
$string['naas_settings_help'] = 'Configure NaaS';
$string['naas_settings_endpoint'] = 'Naas API endpoint';
$string['naas_settings_endpoint_help'] = 'Enter the Naas API endpoint';
$string['naas_settings_username'] = 'Naas API user';
$string['naas_settings_username_help'] = 'Enter the Naas API user';
$string['naas_settings_structure_id'] = 'Naas API structure';
$string['naas_settings_structure_id_help'] = 'Enter the Naas API target structure';
$string['naas_settings_password'] = 'Naas API password';
$string['naas_settings_password_help'] = 'Enter the Naas API password';
$string['naas_settings_timeout'] = 'Naas API timeout';
$string['naas_settings_timeout_help'] = 'Number of seconds to wait before aborting a call to the NaaS API (0 for infinite)';
$string['naas_settings_css'] = 'Naas CSS';
$string['naas_settings_css_help'] = 'Extra CSS to be applied to Naas ressources)';


